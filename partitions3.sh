#!/bin/usr/env bash
###################
#created: by Elena
#topic: fdisk, partitions,mount
#data: 17.10.2020
#version: v1.0.0
##################

echo "type disk name for creation 3 primary partitions"
read name

fdisk /dev/$name << EOF
n
p


+100MB

n
p


+200MB

n
p



w

EOF
echo "you create 3 partitions: sdc1, sdc2, sdc3"
lsblk
echo "create 3 dir: sdc1, sdc2, sdc3"
mkdir /mnt/sdc1
mkdir /mnt/sdc2
mkdir /mnt/sdc3
mkfs.ext4 /dev/sdc1
mkfs.ext4 /dev/sdc2
mkfs.ext4 /dev/sdc3
mount /dev/sdc1 /mnt/sdc1
mount /dev/sdc2 /mnt/sdc2
mount /dev/sdc3 /mnt/sdc3
echo "Done"

