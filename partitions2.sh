#!/bin/usr/env bash
###################
#created: by Elena
#topic: fdisk, partitions,mount
#data: 17.10.2020
#version: v1.0.0
##################

echo "type disk name for creation 2 partitions"
read name

fdisk /dev/$name << EOF
n
p
1

+500MB

n
p
2

+500MB
w
EOF
echo "you create 2 partitions: sdb1, sdb2"
lsblk
echo "create 2 dir: sdb1, sdb2"
mkdir /mnt/sdb1
mkdir /mnt/sdb2
mkfs.ext4 /dev/sdb1
mkfs.ext4 /dev/sdb2
mount /dev/sdb1 /mnt/sdb1
mount /dev/sdb2 /mnt/sdb2

